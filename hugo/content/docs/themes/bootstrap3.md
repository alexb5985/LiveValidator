---
date: 2016-11-30T10:52:24+02:00
description: Bootstrap 3 theme for LiveValidator
menu:
  main:
    parent: 'Themes'
    weight: 1
title: Bootstrap 3
type: theme
theme: Bootstrap3
---
This is the basic Bootstrap 3 theme that does not need jQuery or any other Bootstrap JS components. It uses the `help-block` to display the errors to the users
