---
date: 2016-11-30T10:52:24+02:00
description: Bootstrap 3 Popover theme for LiveValidator
menu:
  main:
    parent: 'Themes'
    weight: 1
title: Bootstrap 3 Popover
type: theme
theme: Bootstrap3Popover
---
This Bootstrap 3 theme uses the popover by Bootstrap to display the errors. It therefore needs jQuery and the Bootstrap JS code.
