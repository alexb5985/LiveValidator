---
date: 2016-11-30T10:52:24+02:00
description: Default theme for LiveValidator
menu:
  main:
    parent: 'Themes'
    weight: 0
title: Default
type: theme
theme: Default
---
This is the default and fallback theme that LiveValidator uses. It is vanilla JS and you will need to also include its CSS. It only provides styling for the inputs and their errors and not any buttons or check/radio boxes.
