var theme = theme || {};
theme.uikit = theme.uikit || {};

theme.uikit.addErrorsSpec = function() {
    beforeEach( function() {
        this.row = helper.themes.uikit.getRow();
        this.input = this.row.querySelector( 'input' );
        this.controls = this.row.querySelector( '.uk-form-controls' );
        this.theme = new LiveValidator.themes.UIkit( this.input );
    } );

    it( 'already has errors', function() {
        this.input.classList.add( 'uk-form-danger' );
        var p = document.createElement( 'p' );
        p.classList.add( 'uk-form-help-block' );
        p.innerHTML = 'Old Error';

        var div = document.createElement( 'div' );
        div.classList.add( 'errors' );
        div.classList.add( 'uk-text-danger' );
        div.classList.add( 'uk-margin-small-left' );
        div.appendChild( p );

        this.controls.appendChild( div );
        expect( this.row ).toContainHtml( '<div class="errors uk-text-danger uk-margin-small-left">' +
        '<p class="uk-form-help-block">Old Error</p></div>' );
        this.theme.addErrors( [ 'New Error' ] );
        expect( this.row ).not.toContainText( 'Old Error' );
        expect( this.row ).toContainHtml( '<div class="errors uk-text-danger uk-margin-small-left">' +
        '<p class="uk-form-help-block">New Error</p></div>' );
        expect( this.input ).toHaveClass( 'uk-form-danger' );
    } );

    it( 'having no errors', function() {
        expect( this.row ).not.toContainElement( '.errors' );
        this.theme.addErrors( [ 'New Error' ] );
        expect( this.row ).toContainHtml( '<div class="errors uk-text-danger uk-margin-small-left">' +
        '<p class="uk-form-help-block">New Error</p></div>' );
        expect( this.input ).toHaveClass( 'uk-form-danger' );
    } );

    it( 'adding multiple errors', function() {
        expect( this.row ).not.toContainElement( '.errors' );
        this.theme.addErrors( [ 'Error 1', 'Error 2' ] );
        expect( this.row ).toContainHtml( '<div class="errors uk-text-danger uk-margin-small-left">' +
        '<p class="uk-form-help-block">Error 1</p><p class="uk-form-help-block">Error 2</p></div>' );
    } );
};
